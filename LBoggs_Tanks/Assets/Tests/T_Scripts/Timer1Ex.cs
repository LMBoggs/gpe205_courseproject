﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer1Ex : MonoBehaviour
{
    //Declare Variables
    public float timerDelay = 1.0f;
    private float nextEventTime;
    
    // Start is called before the first frame update
    void Start()
    {
        //Set variables
        nextEventTime = Time.time + timerDelay;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= nextEventTime)
        {
            Debug.Log("It's me!");
            nextEventTime = Time.time + timerDelay;
        }
    }

}
