﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer2Ex : MonoBehaviour
{
    //Declare variables
    public float timerDelay = 1.0f;
    private float lastEventTime;

    // Start is called before the first frame update
    void Start()
    {
        lastEventTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= lastEventTime + timerDelay)
        {
            Debug.Log("Something snarky");
            lastEventTime = Time.time;
        }
    }
}
