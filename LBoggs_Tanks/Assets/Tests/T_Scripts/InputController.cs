﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public TankData pawn;


    private void Awake()
    {

    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Start with the assumption that I'm not working
        Vector3 directionToMove = Vector3.zero;
        
        // If the W key is down -- Add "forward" to the direction I'm moving
        if (Input.GetKey(KeyCode.W))
        {
            directionToMove += Vector3.forward;
        }

        //If S is down -- Add "reverse" to the direction I'm moving

        if (Input.GetKey(KeyCode.S))
        {
            directionToMove += -Vector3.forward;
        }

        if (Input.GetKey(KeyCode.A))
        {
            pawn.motor.Rotate(-pawn.rotateSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.D))
        {
            pawn.motor.Rotate(pawn.rotateSpeed * Time.deltaTime);
        }


        //After I've checked all my inputs, tell my motor to move in the final direction
        pawn.motor.Move(directionToMove);

    }
}
