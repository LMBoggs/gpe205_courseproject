﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMover : MonoBehaviour
{
    //Declare variables
    private CharacterController characterController;
    private TankData pawn;
    
    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();

        pawn = GetComponent<TankData>();
    }

    public void Move(Vector3 worldDirectionToMove)
    {
        //Calculate local space based on world direction
        Vector3 directionToMove = pawn.tf.TransformDirection(worldDirectionToMove);

        //Move the tank
        characterController.SimpleMove(directionToMove * pawn.moveSpeed);

    }

    public void Rotate(float speed)
    {
        Vector3 rotateVector;

        rotateVector = Vector3.up * speed * Time.deltaTime;

        pawn.tf.Rotate(rotateVector, Space.Self);


    }

    //Used by AI Tanks
    public void RotateTowards(Vector3 lookVector)
    {
        // Find vector to target
        Vector3 VectorToTarget = lookVector;
        //find Quaternion to look down that vector
        Quaternion targetQuaternion = Quaternion.LookRotation(VectorToTarget, Vector3.up);
        //set our rotation to "partway towards" that quaternion
        //pawn.tf.rotation = 
            //Quaternion.RotateTowards(content here)

    }
}


