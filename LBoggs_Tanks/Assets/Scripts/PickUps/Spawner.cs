﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject pickupPrefab;
    private GameObject spawnedPickUp;
    public float spawnDelay;
    private float nextSpawnTime;
    private Transform tf;
    
    // Start is called before the first frame update
    void Start()
    {
        //Get my transform
        tf = gameObject.GetComponent<Transform>();

        //Set next spawn time
        nextSpawnTime = Time.time + spawnDelay;
    }

    // Update is called once per frame
    void Update()
    {
        //If spawner is currently empty
        if (spawnedPickUp == null)
        {
            //Check time to spawn pickup
            if (Time.time > nextSpawnTime)
            {
                //spawn pick up
                spawnedPickUp = Instantiate(pickupPrefab, tf.position, Quaternion.identity); //as GameObject

                //set next time to spawn
                nextSpawnTime = Time.time + spawnDelay;
            }
            
        }

        //else pickup still occupies spawner
        else
        {
            nextSpawnTime = Time.time + spawnDelay;
        }
        
        
    }
}
