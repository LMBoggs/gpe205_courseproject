﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public PowerUp powerup;
    public AudioClip pickupSound;
    
    // Start is called before the first frame update
    void Start()
    {
        //Add this pickup to list on start
        GameManager.instance.ExistingPickUps.Add(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        //Get powerup up controller of foreign object
        PowerUpController powCon = other.GetComponent<PowerUpController>();

        //null check for Powerup controller
        if (powCon != null)
        {
            //Add powerup to foreign object
            powCon.AddPowerUp(powerup);

            //Play Pickup sound
            if (pickupSound != null)
            {
                AudioSource.PlayClipAtPoint(pickupSound, transform.position, 1.0f);
            }
        }

        //Remove from list when destroyed
        GameManager.instance.ExistingPickUps.Remove(this);

        //Destroy this pickup object
        Destroy(gameObject);
    }
}
