﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibleObject : MonoBehaviour
{
    private MeshRenderer myMesh;   
    
    // Start is called before the first frame update
    void Start()
    {
        myMesh = GetComponent<MeshRenderer>();

        myMesh.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
