﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour
{
    public int damageToDeal;
    public float lifeTimer;
    public GameObject shooter;
    private bool dmgComplete = false;

    private GameObject hitObject;
    
    // Start is called before the first frame update
    void Start()
    {
        //Destroy shell x seconds after creation
        Destroy(this.gameObject, lifeTimer);
    }


    //MILESTONE 1 REQUIREMENT: Collide with objects, destroy self on impact
    //----------------------------------------------------
    private void OnCollisionEnter(Collision collision)
    {
        //Store collsion object
        hitObject = collision.gameObject;

        if (!dmgComplete)
        {
            //Deal Damage
            DealDamage();
            dmgComplete = true;
        }


        //Destroy shell
        Destroy(this.gameObject);
    }


    //MILESTONE 1 REQUIREMENT: Deal Damage to Tanks
    //----------------------------------------------------
    void DealDamage()
    {
        //Damage to Tanks
        if (hitObject.GetComponentInParent<TankMotor>()) 
        {
            //Call TakeDamage() on hit Tank, pass in damage and shooter ID
            hitObject.GetComponentInParent<TankMotor>().TakeDamage(damageToDeal, shooter);
                       
        }

        //Damage to Targets
        if (hitObject.GetComponentInParent<Target>())
        {
            hitObject.GetComponentInParent<Target>().TakeDamage(damageToDeal);
        }


    }
}
