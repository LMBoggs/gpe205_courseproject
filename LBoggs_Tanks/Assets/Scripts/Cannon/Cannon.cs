﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Cannon : MonoBehaviour
{
    //VARIABLES
    private TankData pawn;
    private float cooldown = 0f;
    private GameObject ammoToShoot;
    private float myForce;
    //TODO: SoundToPlay via AudioManager

    //TODO: Organize Inspector


    //Basic Shell
    public GameObject basicShellPF;
    public float basicShellForce;
    public float basicChamberTime = 1.0f;

    //Quick Shell
    public GameObject quickShellPF;
    public float quickShellForce;
    public float quickChamberTime = 0.5f;

    //Show Shell
    public GameObject slowShellPF;
    public float slowShellForce;
    public float slowChamberTime = 1.5f;

    //----------------------------------------
        
    
    // Start is called before the first frame update
    void Start()
    {
        pawn = GetComponent<TankData>();

        //Set up code with shot values
        SetShotType(pawn.myShot);

        //no cooldown on Start
        cooldown = 0f;

    }

    // Update is called once per frame
    void Update()
    {
        //Deduct cooldown timer
        cooldown -= Time.deltaTime;

    }

    public void Shoot()
    {
        GameObject shell;
        
        //If cooldown is ready, shoot
        if (cooldown <= 0)
        {
            //Create shell at bullet origin and rotation, assign to variable
            shell = Instantiate(ammoToShoot, new Vector3(pawn.bulletOrigin.position.x, pawn.bulletOrigin.position.y, pawn.bulletOrigin.position.z), Quaternion.LookRotation(pawn.bulletOrigin.forward, pawn.bulletOrigin.up));


            //Apply force based on pawn forward and shell-determined force
            shell.GetComponent<Rigidbody>().AddForce(pawn.tf.forward * myForce);


            //Identify self as shell shooter
            shell.GetComponent<Shell>().shooter = this.gameObject;

            //Reset cooldown
            ResetCooldown();

            //TODO: play sound, adjust visuals, adjust ammo count
        }

        //else cooldown in progress, no shoot
        else
        {
            //TODO: play error sound & visuals
            //Resume normal visuals
        }
    }


    public void SetShotType(TankData.shotType newShotType)
    {
        //Set shot type on pawn
        pawn.myShot = newShotType;

        //Update cannon values based on shot type
        switch (pawn.myShot)
        {
            //Basic shell
            case TankData.shotType.basicShell:
                ammoToShoot = basicShellPF; //use basic shell prefab
                myForce = basicShellForce; //use basic shell force
                cooldown = basicChamberTime; //use basic shell cooldown
                //Set damage
                break;

            //Quick Shell
            case TankData.shotType.quickShell:
                ammoToShoot = quickShellPF; // use quick shell prefab
                myForce = quickShellForce; //use quick shell force
                cooldown = quickChamberTime; // use quick shell cooldown
                //Set damage
                break;

            //Slow Shell
            case TankData.shotType.slowShell:
                ammoToShoot = slowShellPF; // use slow shell prefab
                myForce = slowShellForce; // use slow shell force
                cooldown = slowChamberTime; // use slow shell cooldown
                //set damage
                break;

            //Default: Basic Shell
            default:
                ammoToShoot = basicShellPF; //use basic shell prefab
                myForce = basicShellForce; //use basic shell force
                cooldown = basicChamberTime; //use basic shell cooldown
                //Set damage
                break;
        }
    }

    public void ResetCooldown()
    {
        switch (pawn.myShot)
        {
            //Basic Shell Cooldown
            case TankData.shotType.basicShell:
                cooldown = basicChamberTime;
                break;

            //Quick Shell Cooldown
            case TankData.shotType.quickShell:
                cooldown = quickChamberTime;
                break;

            //Slows Shell Cooldown
            case TankData.shotType.slowShell:
                cooldown = slowChamberTime;
                break;

            //Default: basic shell cooldown
            default:
                cooldown = basicChamberTime;
                break;
        }
    }
}
