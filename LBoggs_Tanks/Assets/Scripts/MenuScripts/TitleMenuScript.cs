﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleMenuScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartBtn()
    {
        SceneManager.LoadScene("MainLevel");
    }

    public void OptionsBtn()
    {
        SceneManager.LoadScene("OptionsMenu");
    }

    public void QuitBtn()
    {
        Debug.Log("You have quit the game.");

        Application.Quit();
    }
}
