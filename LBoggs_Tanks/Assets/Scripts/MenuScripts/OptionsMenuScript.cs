﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OptionsMenuScript : MonoBehaviour
{
    public Button RandMapBtn;
    public Button DailyMapBtn;
    public Button SeedMapBtn;
    public InputField SeedInput;   
           
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


    }

    public void RandBtn()
    {
        //Set Map type in game manager
        GameManager.instance.maptype = GameManager.MapTypes.Random;

        //change other button colors
    }

    public void SeedMap()
    {
        //Set Map type in game manager
        GameManager.instance.maptype = GameManager.MapTypes.SpecificSeed;

        //Set seed to input field
        GameManager.instance.specificMapSeed = int.Parse(SeedInput.text);

        //change other button colors
    }

    public void DailyMap()
    {
        //Set Map type in game manager
        GameManager.instance.maptype = GameManager.MapTypes.MapOfTheDay;

        //change other button colors
    }

    public void SFX_Up()
    {
        //if volume is not max, volume ++

        //if volume is at max, set semitransparent button
    }

    public void SFX_Down()
    {
        //if volume is > 0, volume --

        //if volume is at 0, set semitransparent button
    }

    public void Music_Up()
    {
        //if volume is not max, volume ++

        //if volume is at max, set semitransparent button
    }

    public void Music_Down()
    {
        //if volume is > 0, volume --

        //if volume is at 0, set semitransparent button
    }

    public void BackBtn()
    {
        SceneManager.LoadScene("TitleScreen");
    }
}
