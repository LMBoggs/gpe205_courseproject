﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellPad : MonoBehaviour
{
    //Variables
    public TankData pawn;
    private Cannon pawnCannon;

    public enum PadShotTypes
    {
        basicShell,
        quickShell,
        slowShell,
    }

    public PadShotTypes padShotType = PadShotTypes.basicShell;
    
    // Start is called before the first frame update
    void Start()
    {
        pawnCannon = pawn.GetComponent<Cannon>();
    }

    private void OnTriggerEnter(Collider other)
    {
        //if pawn is the collider
        if (other.gameObject == pawn.gameObject)
        {
            //Set pawn shot type to pad shot type
            switch (padShotType)
            {
                //Basic
                case PadShotTypes.basicShell:
                    pawn.cannon.SetShotType(TankData.shotType.basicShell);
                    break;

                //Quick
                case PadShotTypes.quickShell:
                    pawn.cannon.SetShotType(TankData.shotType.quickShell);
                    break;

                //Slow
                case PadShotTypes.slowShell:
                    pawn.cannon.SetShotType(TankData.shotType.slowShell);
                    break;

                //Default, Basic
                default:
                    pawn.cannon.SetShotType(TankData.shotType.basicShell);
                    break;
            }
        }
    }
}
