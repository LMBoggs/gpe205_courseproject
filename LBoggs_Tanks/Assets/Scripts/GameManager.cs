﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public List<TankData> EnemyTanks;
    public PlayerController playerOneControl;
    public TankData playerOneTank;

    [Tooltip("0 is no sound lost over distance.")]
    public float SoundLossPerMeter = 0.0f;

    //exposed list of all pick ups spawned into level (Req. M3)
    public List<PickUp> ExistingPickUps = new List<PickUp>();

    public enum MapTypes
    {
        Random,
        MapOfTheDay,
        SpecificSeed
    }

    public MapTypes maptype;

    public int specificMapSeed;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        else
        {
            Debug.Log("A second Game Manager was detected and destroyed.");
            Destroy(this.gameObject);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        GetPlayerOneTank();
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    void GetPlayerOneTank()
    {
        playerOneTank = playerOneControl.pawn;
    }
}
