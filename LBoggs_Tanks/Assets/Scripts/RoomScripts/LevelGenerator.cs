﻿//using System;

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class LevelGenerator : MonoBehaviour
{
    //A lot of this code will need to be cut up, some of it belongs in the 
    //GameManager and other places.

    //List of prefabs
    public List<GameObject> roomPrefabs;
    public GameObject[,] grid;

    public int numCols;
    public int numRows;

    //This could also be just one Vector 2
    public float tileWidth = 50.0f; // make sure you know how big your room is. Consider Probuilder
    public float tileHeight = 50.0f;

    //Seed for map generation
    public int mapSeed;

    // Start is called before the first frame update
    void Start()
    {
        BuildLevel();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void BuildLevel()
    {
        //Seed the random generator based on GameManager enum selection
        switch (GameManager.instance.maptype)
        {
            case GameManager.MapTypes.MapOfTheDay:
                mapSeed = DateToInt(DateTime.Now.Date);
                break;

            case GameManager.MapTypes.Random:
                mapSeed = DateToInt(DateTime.Now);
                break;

            case GameManager.MapTypes.SpecificSeed:
                mapSeed = GameManager.instance.specificMapSeed;
                break;

            default:
                mapSeed = DateToInt(DateTime.Now);
                break;
        }
        
        Random.seed = mapSeed;
        
        //Create 2D array
        grid = new GameObject[numCols, numRows];

        //For loops through all the rooms
        for (int currentCol = 0; currentCol < numCols; currentCol++)
        {
            for (int currentRow = 0; currentRow < numRows; currentRow++)
            {
                //Instantiate a room
                GameObject tempRoom = Instantiate(RandomRoom());

                //Store it in the array
                grid[currentCol, currentRow] = tempRoom;

                //Move it into the position
                tempRoom.transform.position = new Vector3(currentCol * tileWidth, 0.0f, -currentRow * tileHeight); //negative currentRow is to control if the rows spawn towards us or away from us

                //Name my room (prevents "Clone..." naming)
                tempRoom.name = "Room (" + currentCol + ", " + currentRow + ")";

                //Make it a child of this Level Generator object
                tempRoom.transform.parent = this.gameObject.transform;

                //Based on it's location, open the appropriate doors
                //Optimize: Move this snipet into a different function, call it after the maze is created.
                //Extras idea: set the color material on locked doors(lead to the abbys). Could be same material as wall mat.
                // Cont: Put triggers on all !locked doors to open when player or AI passes through.
                //Could put buttons in level that control doors also.

                Room roomScript = tempRoom.GetComponent<Room>();
                
                //Open all west doors except the far west rooms
                if (currentCol != 0)
                {
                    roomScript.doorWest.SetActive(false);
                }

                //Open all east doors except the far east rooms
                if (currentCol != numCols - 1)
                {
                    roomScript.doorEast.SetActive(false);
                }

                //Open all north doors except the far north
                if (currentRow != 0)
                {
                    roomScript.doorNorth.SetActive(false);
                }

                //Open all south doors except the far south
                if (currentRow != numRows - 1)
                {
                    roomScript.doorSouth.SetActive(false);
                }
            }
        }

    }

    public void DestroyLevel()
    {
        for (int currentRow = 0; currentRow < numRows; currentRow++)
        {
            for (int currentCol = 0; currentCol < numCols; currentCol++)
            {
                Destroy(grid[currentCol, currentRow]);
            }
        }
    }

    private GameObject RandomRoom()
    {
        int roomIndex = Random.Range(0, roomPrefabs.Count);
        return roomPrefabs[roomIndex];
    }

    public int DateToInt(DateTime dateToUse)
    {
        //Add data together and return the sum
        return dateToUse.Year + dateToUse.Month + dateToUse.Day
               + dateToUse.Hour + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }
}
