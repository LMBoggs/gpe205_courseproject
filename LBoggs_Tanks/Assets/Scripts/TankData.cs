﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{
    //Variables
    public bool isEnemy = true;
    public int health;
    public int maxHealth;

    [Tooltip("Sound after distance must be above this number for Tank to hear it. 0 is perfect hearing.")]
    public float hearingSensitivity = 0.0f;

    [Tooltip("Only sounds made within this radius can be heard.")]
    public float hearingRadius = 5.0f;

    public float fieldOfView = 45.0f;
    public float sightRange = 30.0f; 

    public float moveSpeed;
    public float reverseMoveSpeed;
    [Tooltip("How much to multiply base speed to get boost speed. speed *= boostModifier ")]
    public float boostModifier = 1.0f;
    [Tooltip("How much volume will be added while boosting")]
    public float boostVolumeAdd = 2.0f;
    public bool isBoosting = false;
    public float rotateSpeed;
    public float shotsPerSecond;
    public Transform bulletOrigin;
    public GameObject lastShotBy;

    public enum shotType
    {
        basicShell, //base damage, base speed
        quickShell, //low damage, high speed
        slowShell //high damage, low speed
    }

    public shotType myShot;

    public Transform tf;
    public TankMotor motor;
    public Cannon cannon;


    void Awake()
    {
        //Store this tank's transform and motor components
        tf = GetComponent<Transform>();
        motor = GetComponent<TankMotor>();
        cannon = GetComponent<Cannon>();
    }
   
}
