﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DumbAIController : AIController
{
    public Transform target;
    
    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        Seek(target);
    }
}
