﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    //Variabls
    public TankData pawn;
    public NoiseMaker pawnNoiseMaker;
    private float startVolume = 1.0f;

    private void Start()
    {
        //Get the noisemaker component
        pawnNoiseMaker = pawn.GetComponent<NoiseMaker>();

        //Save the volume we start with (will be used to reset noisemaker volume to normal)
        if (pawnNoiseMaker != null)
        {
            startVolume = pawnNoiseMaker.volume;
        }
        
    }

    void Update()
    {
        //Begin frame with 0 values in movement
        Vector3 directionToMove = Vector3.zero;
       

        //Add "forward" value while key pressed
        if (Input.GetKey(KeyCode.W))
        {
            directionToMove += Vector3.forward;
        }

        //"Reverse" (deduct forward value) while key is pressed
        if (Input.GetKey(KeyCode.S))
        {
            directionToMove -= Vector3.forward;
        }

        //Turn left while key is pressed
        if (Input.GetKey(KeyCode.A))
        {
            pawn.motor.Rotate(-pawn.rotateSpeed);
        }

        //Turn right while key is pressed
        if (Input.GetKey(KeyCode.D))
        {
            pawn.motor.Rotate(pawn.rotateSpeed);
        }

        //Boost while left shift is pressed
        if (Input.GetKey(KeyCode.LeftShift))
        {
            pawn.isBoosting = true;
        }

        //if !Left Shift, isBoosting is false
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            pawn.isBoosting = false;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            pawn.cannon.Shoot();
        }

        //Call Move() after all inputs determined
        pawn.motor.Move(directionToMove);

        //Adjust volume each frame
        //--------------------------
        //If player is not moving, subtract volume each frame
        if (directionToMove == Vector3.zero)
        {
            //If volume is positive, disipate
            if (pawnNoiseMaker.volume > 0)
            {
                //Subtract 1 sound point each second
                pawnNoiseMaker.volume -= Time.deltaTime;
            }

            //if volume goes negative, set to 0
            if (pawnNoiseMaker.volume < 0)
            {
                pawnNoiseMaker.volume = 0.0f;
            }
            
        }

        //But if they are moving
        else
        {
            //if player is boosting, add boost volume
            if (pawn.isBoosting)
            {
                pawnNoiseMaker.volume = startVolume + pawn.boostVolumeAdd;
            }

            //else moving standard
            else
            {
                pawnNoiseMaker.volume = startVolume;
            }
        }

    }

}
