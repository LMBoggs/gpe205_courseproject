﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration.Assemblies;
using UnityEngine;

public class PatrolAIController : AIController
{

    // Start is called before the first frame update
    new void Start()
    {
        //Start of AIController parent
        base.Start();

        //TODO: Make current waypoint that which is closest on start

        //Start at first waypoint in array
        currentWayPoint = 0;
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case AIStates.Idle:
                //Do a special "Update" for that state
                Idle();
                //check for transitions
                if (Time.time > stateStartTime + 3.0f)
                {
                    ChangeState(AIStates.Patrol);
                }

                break;
            case AIStates.Patrol:
                //If occupied w/ avoid state, do avoidance
                if (currentAvoidState != AIAvoidState.None)
                {
                    DoAvoidance();
                }

                //else Patrol
                else
                {
                    Patrol();
                }
                
                //Check for transitions
                //TODO: Add some transitions

                break;
                
        }
    }

    
}
