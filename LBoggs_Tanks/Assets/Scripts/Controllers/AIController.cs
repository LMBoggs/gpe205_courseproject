﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public abstract class AIController : MonoBehaviour
{
    //Variables
    public TankData pawn;

    [Tooltip("How long the tank should stay in the idle state before resuming tasks.")]
    public float idleTime = 1.0f;

    //Patrol variables
    public enum LoopType { Loop, Stop, PingPong, Random }
    public List<Transform> waypoints;
    public int currentWayPoint;
    public float minDistance;
    public bool isForward = true;

    public LoopType loopType;

    //Flee variables
    public float fleeDistance = 1.0f;

    //Investigate variables
    protected Vector3 lastHeardSoundPoint;
    public float investigateExit = 1.0f; //exit time after idle at sound point
    protected float investigateTimer; //timer tracker

    //State Machine variables
    public float stateStartTime;
    public AIStates currentState;
    public AIAvoidState currentAvoidState;

    public enum AIStates
    {
        Idle,
        Patrol,
        Chase,
        Flee,
        Investigate,
        Glare
    }

    public enum AIAvoidState
    {
        None,
        TurnToAvoid,
        MoveToAvoid,
    }

    //Avoidance variables
    public float feelerDistance = 2.0f;
    public float avoidMoveTime = 2.0f;
    private float exitTime;

    public void Start()
    {
        AddToEnemyList();
        SetIsEnemy(true);

        //Store exitTime as avoidMoveTime, will be used to reset
        exitTime = avoidMoveTime;

        //if TankData not connectd, use component on this object
        if (pawn == null)
        {
            pawn = GetComponent<TankData>();
        }

    }



    public void ChangeState(AIStates newState)
    {
        stateStartTime = Time.time;
        currentState = newState;
        ChangeAvoidState(AIAvoidState.None);
    }
    
    public bool IsBlocked()
    {
        RaycastHit hit;

        if (Physics.Raycast(pawn.tf.position, pawn.tf.forward, out hit, feelerDistance))
        {
            //ignore if raycast hits player
            if (hit.collider.CompareTag("Player"))
            {
                return false;
            }

            //Debug.Log(hit.collider.name + " is blocking me.");

            //Non-player collider is blocking tank
            return true;

        }

        //true if not the player;
        return false; 

    }

    private void OnDrawGizmosSelected()
    {
        if (!CanHear(GameManager.instance.playerOneTank.gameObject))
        {            
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(pawn.tf.position, pawn.hearingRadius);
        }

        else
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(pawn.tf.position, pawn.hearingRadius);
        }
    }


    public bool CanHear(GameObject other)
    {                
               
        //Get the target's NoiseMaker
        NoiseMaker noiseMaker = other.GetComponent<NoiseMaker>();               

        //If they don't have one, return false
        if (noiseMaker == null)
        {
            return false;
        }

        //else, has noisemaker
        else
        {
            //Get distance to target
            Vector3 distanceToSound = other.GetComponent<Transform>().position - pawn.tf.position;

            //If too far away, return false
            if (distanceToSound.magnitude > pawn.hearingRadius)
            {
                return false;
            }
            
            //Adjust volume over distance
            float adjustedVolume = noiseMaker.volume;
            adjustedVolume -= (distanceToSound.magnitude * GameManager.instance.SoundLossPerMeter); 
            
            //If the volume is loud enough, update lastHeardObejct and return true
            if (adjustedVolume > pawn.hearingSensitivity)
            {
                //Debug.Log("I heard you, " + other.name);
                //Debug.Log("Your distance from me is: " + distanceToSound.magnitude);

                //update last sound point at the time of sound heard
                lastHeardSoundPoint = other.transform.position;

                return true;
            }

            //else too quiet
            else
            {
                return false;
            }
        }
       
    }

    public bool CanSee(GameObject other)
    {
        //Find target's transform component
        Transform otherTf = GetComponent<Transform>();

        //Get vector to target
        Vector3 vectorToTarget = other.GetComponent<Transform>().position - pawn.tf.position;
        //vectorToTarget.Normalize();

        //Get angle between my forward and vector to target
        float angleToTarget = Vector3.Angle(vectorToTarget, pawn.tf.forward);
        
        //If less than FOV, return true
        if (angleToTarget < pawn.fieldOfView)
        {
            //Debug.Log("You're in my field of view");
           
            //Raycast for blockers / cover
            Ray ray = new Ray(pawn.tf.position, vectorToTarget);
            RaycastHit hitInfo;

            //If can see object
            if (Physics.Raycast(ray, out hitInfo, pawn.sightRange))
            {
                //Debug.DrawLine(ray.origin, pawn.tf.position, Color.green);
                Debug.DrawRay(ray.origin, vectorToTarget, Color.green);
                //Debug.Log(hitInfo.collider.gameObject.name);

                //If hit target, return true
                if (hitInfo.collider.gameObject == other)
                {
                    return true;
                }

                //If hit child of the object, return true (needed for child collisons!)
                if (hitInfo.collider.gameObject.transform.IsChildOf(other.gameObject.transform))
                {
                    return true;
                }

            }
            
            //else target is behind cover, return false
            return false;
        }

        //else out of sight, return false
        else
        {
            return false;
        }
        
    }


    public void Idle()
    {
        //Do nothing
    }

    public void  Seek(Transform target)
    {
        //Get vector to target
        Vector3 targetVector = (target.transform.position - pawn.transform.position).normalized;

        //Rotate towards target
        pawn.motor.RotateTowards(targetVector);

        //If not blocked, move towards target
        if (!IsBlocked())
        {
            pawn.motor.Move(Vector3.forward);
        }

        //else blocked, change avoid state
        else
        {
            ChangeAvoidState(AIAvoidState.TurnToAvoid);
        }
        
    }
    
    //Can use to investigate sound. Set z or y to be the tanks position, so no upward/downward saw occurs
    public void SeekPoint(Vector3 targetPoint)
    {
        //Get vector to point
        Vector3 targetVector = (targetPoint - pawn.tf.position).normalized;

        //Rotate towards point
        pawn.motor.RotateTowards(targetVector);

        //If not blocked, move towards point
        if (!IsBlocked())
        {
            pawn.motor.Move(Vector3.forward);
        }

        //else, do avoidance
        else
        {
            ChangeAvoidState(AIAvoidState.TurnToAvoid);
        }
        
    }

    public void Flee(Transform target)
    {
        //Vector to target
        Vector3 targetVector = (target.position - pawn.tf.position).normalized;

        //Vector away from target
        Vector3 awayVector = -targetVector;

        //Find flee distance along awayVector
        awayVector *= fleeDistance;

        //Find point in space to flee to
        Vector3 fleePoint = pawn.tf.position + awayVector;

        //SeekPoint (this function handles rotation and blockages)
        SeekPoint(fleePoint);
        
        //Debug.Log("My flee point is : " + fleePoint);
    }

    //PATROL
    //--------------------------
    public void Patrol()
    {
        //Seek my targeted waypoint (this function handles rotation, blocks and movement)
        Seek(waypoints[currentWayPoint]);

        //if close enough to waypoint, pick new waypoint
        if (Vector3.Distance(pawn.tf.position, waypoints[currentWayPoint].position) < minDistance)
        {
            //Going forward through list
            if (isForward)
                //Advance to next waypoint
                currentWayPoint++;
            
            //Going backward through list (used for PingPong)
            else
            {
                currentWayPoint--;
            }

            //TODO: Make special case for Random selection (wandering), not ++ or --


            //If waypoint index exceeds array bounds, adjust waypoint selection
            if (currentWayPoint >= waypoints.Count || currentWayPoint < 0)
            {
                if (loopType == LoopType.Loop)
                {
                    currentWayPoint = 0;
                }

                else if (loopType == LoopType.Random)
                {
                    currentWayPoint = Random.Range(0, waypoints.Count);
                }

                else if (loopType == LoopType.PingPong)
                {
                    //Flip the bool
                    isForward = !isForward;

                    //If index is beyond count, set to last waypoint in array (start going backwards through list)
                    if (currentWayPoint >= waypoints.Count)
                    {
                        currentWayPoint = waypoints.Count - 1;
                    }

                    //If index is less than 0, set to 0 (start going foward through list)
                    else
                    {
                        currentWayPoint = 0;
                    }
                }
            }
        }
    }

    
    void ChangeAvoidState (AIAvoidState newState)
    {
        currentAvoidState = newState;
    }

    public void DoAvoidance()
    {
        switch (currentAvoidState)
        {
            case AIAvoidState.None:
                break;

            case AIAvoidState.TurnToAvoid:
                //Rotate
                pawn.motor.Rotate(pawn.rotateSpeed); //TODO: Make this a variable

                //if not blocked, move
                if (!IsBlocked())
                {
                    //Reset avoidTime to start value
                    avoidMoveTime = exitTime;

                    ChangeAvoidState(AIAvoidState.MoveToAvoid);
                }

                break;

            case AIAvoidState.MoveToAvoid:
                if (!IsBlocked())
                {
                    //Move forward
                    pawn.motor.Move(Vector3.forward);

                    //Deduct from timer
                    avoidMoveTime -= Time.deltaTime;

                    //When timer is 0, return to no avoid state
                    if (avoidMoveTime <= 0)
                    {
                        ChangeAvoidState(AIAvoidState.None);
                    }
                }

                //else blocked, turn to avoid
                else
                {
                    ChangeAvoidState(AIAvoidState.TurnToAvoid);
                }
                
                break;
        }
    }

    public void AddToEnemyList()
    {
        GameManager.instance.EnemyTanks.Add(pawn);
    }

    public void SetIsEnemy(bool status)
    {
        pawn.isEnemy = status;
    }


}
