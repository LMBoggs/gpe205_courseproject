﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatController : MonoBehaviour
{
    public PowerUpController powCon;
    public PowerUp cheatPowerUp;
    
    // Start is called before the first frame update
    void Start()
    {
        //if no power controller assigned, get the component from this object
        if (powCon == null)
        {
            powCon = GetComponent<PowerUpController>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Add a powerup when player presses this key
        if (Input.GetKeyDown(KeyCode.Y))
        {
            powCon.AddPowerUp(cheatPowerUp);
        }
    }
}
