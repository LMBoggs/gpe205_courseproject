﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseAIController : AIController
{
    // Start is called before the first frame update
    new void Start()
    {
        //Start of AIController parent
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {            
            case AIStates.Idle:
                //Do a special "Update" for that state
                Idle();
                //check for transitions
                if (Time.time > stateStartTime + 3.0f)
                {
                    ChangeState(AIStates.Chase);
                }

                break;
            case AIStates.Chase:
                //If occupied w/ avoid state, do avoidance
                if (currentAvoidState != AIAvoidState.None)
                {
                    DoAvoidance();
                }

                //else chase
                else
                {
                    //CHASE (Seek the player's position)
                    Seek(GameManager.instance.playerOneTank.transform);
                }
                
                //Check for transitions
                //TODO: Add some transitions

                break;
        }
    }
}
