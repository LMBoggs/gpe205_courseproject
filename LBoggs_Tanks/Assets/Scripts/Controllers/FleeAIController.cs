﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FleeAIController : AIController
{
    // Start is called before the first frame update
    new void Start()
    {
        //Start of AIController parent
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case AIStates.Idle:
                //Do a special "Update" for that state
                Idle();
                //check for transitions
                if (Time.time > stateStartTime + 3.0f)
                {
                    ChangeState(AIStates.Flee);
                }

                //Check for transitions
                //Player is too close               
                if (Vector3.Distance(pawn.tf.position, GameManager.instance.playerOneTank.transform.position) <= fleeDistance)
                {
                    ChangeState(AIStates.Flee);
                }

                break;
            case AIStates.Flee:
                //If occupied w/ avoid state, do avoidance
                if (currentAvoidState != AIAvoidState.None)
                {
                    DoAvoidance();
                }

                //else flee
                else
                {
                    //Flee from player
                    Flee(GameManager.instance.playerOneTank.transform);
                }

                //Check for transitions
                //Far enough from player >> IDLE              
                if (Vector3.Distance(pawn.tf.position, GameManager.instance.playerOneTank.transform.position) > fleeDistance)
                {
                    ChangeState(AIStates.Idle);
                }

                //timer to break flee?
                //TODO: Add some transitions

                

                break;
        }
    }
}
