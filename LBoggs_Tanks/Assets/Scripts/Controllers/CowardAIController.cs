﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CowardAIController : AIController
{
    // Start is called before the first frame update

    
    new void Start()
    {
        base.Start();   
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            //IDLE
            case AIStates.Idle:
                //Do a special "Update" for that state
                Idle();

                //Transitions
                //--------------

                //Time's up --> Patrol
                if (Time.time > stateStartTime + idleTime)
                {
                    ChangeState(AIStates.Patrol);
                }

                //Sees Player --> Flee
                if (CanSee(GameManager.instance.playerOneTank.gameObject))
                {
                    //Debug.Log("I see you, player!");
                    ChangeState(AIStates.Flee);
                }

                //Hears Player --> Investigate
                if (CanHear(GameManager.instance.playerOneTank.gameObject))
                {
                    //set timer
                    investigateTimer = investigateExit;

                    //Change to Investigate State
                    ChangeState(AIStates.Investigate);
                }



                break;

            //PATROL
            case AIStates.Patrol:
                //If occupied w/ avoid state, do avoidance
                if (currentAvoidState != AIAvoidState.None)
                {
                    DoAvoidance();
                }

                //else Patrol
                else
                {
                    Patrol();
                }

                //Transitions
                //--------------

                //Sees Player --> Flee
                if (CanSee(GameManager.instance.playerOneTank.gameObject))
                {
                    //Debug.Log("I see you, player!");
                    ChangeState(AIStates.Flee);
                }

                //Hears Player --> Investigate
                if (CanHear(GameManager.instance.playerOneTank.gameObject))
                {
                    //set timer
                    investigateTimer = investigateExit;

                    //Change to Investigate State
                    ChangeState(AIStates.Investigate);
                                     
                }

                break;

            //INVESTIGATE
            case AIStates.Investigate:
                               
                //If occupied w/ avoid state, do avoidance
                if (currentAvoidState != AIAvoidState.None)
                {
                    DoAvoidance();
                }

                //else investigate
                else
                {
                    //Transitions take priority in this state
                    //--------------------------------------------

                    //Sees Player --> Flee
                    if (CanSee(GameManager.instance.playerOneTank.gameObject))
                    {                        
                        ChangeState(AIStates.Flee);
                    }

                    
                    //No Transitions found, do Investigate
                    //----------------------------------------------------
                    //If soundpoint not yet reached
                    if (Vector3.Distance(pawn.tf.position, lastHeardSoundPoint) > minDistance)
                    {
                        //Seek Last heard soundpoint
                        SeekPoint(lastHeardSoundPoint);

                        //Set timer to full
                        investigateTimer = investigateExit;
                    }

                    //Hears New Sound --> Investigate Loop
                    if (CanHear(GameManager.instance.playerOneTank.gameObject))
                    {
                        //set timer
                        investigateTimer = investigateExit;

                        //Change to Investigate State
                        ChangeState(AIStates.Investigate);

                    }

                    //When soundpoint reached, deduct from timer
                    if (Vector3.Distance(pawn.tf.position, lastHeardSoundPoint) < minDistance)
                    {
                        investigateTimer -= Time.deltaTime;
                        Idle();
                        
                    }

                    //End of timer reached w/o transitions detected, resume Patrol
                    if (investigateTimer <= 0)
                    {
                        ChangeState(AIStates.Patrol);
                    }

                    
                }
                                
                break;


            //Flee
            case AIStates.Flee:
                //If occupied w/ avoid state, do avoidance
                if (currentAvoidState != AIAvoidState.None)
                {
                    DoAvoidance();
                }

                //else flee
                else
                {
                    //Flee from player
                    Flee(GameManager.instance.playerOneTank.transform);
                }

                //Transitions
                //--------------

                //Far enough from player >> Resume Patrol              
                if (Vector3.Distance(pawn.tf.position, GameManager.instance.playerOneTank.transform.position) > fleeDistance)
                {
                    ChangeState(AIStates.Patrol);
                }

                
                               

                break;
                                
            
        }
    }
}
