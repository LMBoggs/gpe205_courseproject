﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_M1 : AIController
{
    private Cannon pawnCannon;
    public float shootDelay;
    private float shootReset;

    
    // Start is called before the first frame update
    new void Start()
    {
        //Add self to enemy list, set isEnemy to true
        base.Start();

        pawnCannon = pawn.GetComponent<Cannon>();

        //Store delay on start as reset
        shootReset = shootDelay;
    }

    // Update is called once per frame
    void Update()
    {
        //Deduct shootDelay timer
        shootDelay -= Time.deltaTime;

        //Shoot when timer is 0
        if (shootDelay <= 0)
        {
            pawnCannon.Shoot();

            //Reset Timer
            shootDelay = shootReset;
        }
    }
}
