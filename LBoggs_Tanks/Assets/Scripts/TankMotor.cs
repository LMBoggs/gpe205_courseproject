﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour
{
    
    //Variables
    private CharacterController characterController;
    private TankData pawn;

    
    // Start is called before the first frame update
    void Start()
    {
        //store character controller component
        characterController = GetComponent<CharacterController>();

        //store TankData component
        pawn = GetComponent<TankData>();
    }

    public void Move(Vector3 worldDirectionToMove)
    {
        float calculatedSpeed = pawn.moveSpeed;
        
        //If player is moving forward, use moveSpeed
        if (worldDirectionToMove.z >= 0)
        {
            calculatedSpeed = pawn.moveSpeed;

            //if boosting, multiply by modifier 
            if (pawn.isBoosting)
            {
                calculatedSpeed = pawn.moveSpeed * pawn.boostModifier;
            }
        }

        //if player is reversing, use reverse speed
        else
        {
            calculatedSpeed = pawn.reverseMoveSpeed;

            //if boosting, multiply by modifier 
            if (pawn.isBoosting)
            {
                calculatedSpeed = pawn.reverseMoveSpeed * pawn.boostModifier;
            }
        }


        //Convert world space to local space
        Vector3 directionToMove = pawn.tf.TransformDirection(worldDirectionToMove);


        //Move tank in local space
        characterController.SimpleMove(directionToMove * calculatedSpeed);
    }

    public void Rotate (float speed)
    {
        Vector3 rotateAxis;

        //Set tank to rotate around it's "up" axis, at speed per second
        rotateAxis = Vector3.up * speed * Time.deltaTime;
       
        pawn.tf.Rotate(rotateAxis, Space.Self);
    }

    //Used by AI Tanks
    public void RotateTowards(Vector3 lookVector)
    {
        Quaternion targetRotation = Quaternion.LookRotation(lookVector);
        pawn.tf.rotation = Quaternion.RotateTowards(pawn.tf.rotation, targetRotation, pawn.rotateSpeed * Time.deltaTime);
    }

    public void TakeDamage(int dmg, GameObject shooter)
    {
        //Store who shot me
        pawn.lastShotBy = shooter;
        
        //Decrease health by damage
        pawn.health -= dmg;

        if (pawn.health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        //if Tank is enemy tank, remove from list
        if (pawn.isEnemy)
        {
            GameManager.instance.EnemyTanks.Remove(pawn);
        }

        Destroy(this.gameObject);

        //TODO: GameOver via player death, all enemies dead
    }

}
