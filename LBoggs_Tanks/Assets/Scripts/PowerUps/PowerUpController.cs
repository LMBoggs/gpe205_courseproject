﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;


public class PowerUpController : MonoBehaviour
{
    private TankData pawn;

    public List<PowerUp> powerUps;
    //Do not need to initialize when public, doing so for practice though
    
    // Start is called before the first frame update
    void Start()
    {
        //Initalize powerUps list
        powerUps = new List<PowerUp>();   

        //Get TankData component
        pawn = GetComponent<TankData>();
    }

    // Update is called once per frame
    void Update()
    {
        //List to hold expired powerups
        List<PowerUp> expiredPowerups = new List<PowerUp>();

        //Update timers on all powerups
        foreach (PowerUp pwr in powerUps)
        {
            //decrement timer
            pwr.duration -= Time.deltaTime;

            //Add to expired list if expired
            if (pwr.duration <= 0)
            {
                expiredPowerups.Add(pwr);
            }
        }

        //Remove expired powerups from main list
        foreach (PowerUp pwr in expiredPowerups)
        {
            //Call deactivate func.
            pwr.OnDeactivate(pawn);

            //Remove from list
            powerUps.Remove(pwr);
        }

        //Clear list for good measure (practice)
        expiredPowerups.Clear();

    }

    public void AddPowerUp(PowerUp newPowerUp)
    {
        //Call OnActivate event
        newPowerUp.OnActivate(pawn);

        //Add nonpermanent Powerups to the list
        if (!newPowerUp.isPermanent)
        {
            powerUps.Add(newPowerUp);
        }
        


    }

    public void RemovePowerUp(PowerUp oldPowerUp)
    {
        //Call OnRemove
        //oldPowerUp.OnRemovePowerUp(this.gameObject);
        oldPowerUp.OnDeactivate(pawn);

        //Remove from list
        powerUps.Remove(oldPowerUp);
    }
}
