﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class SpeedPowerUp : PowerUp
{
    public float boostAmount;
    public float timeRemaining;

    public void OnApplyPowerUp(GameObject target)
    {
        TankData tempData = target.GetComponent<TankData>();

        tempData.moveSpeed += boostAmount;

    }

    public void OnRemovePowerUp(GameObject target)
    {
        TankData tempData = target.GetComponent<TankData>();
        tempData.moveSpeed -= boostAmount;
    }

}
