﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class PowerUp
{
    public float speedModifier;
    public int healthModifier;
    public int maxHealthModifier;
    public float fireRateModifier;

    public float duration;

    public bool isPermanent;

    //public abstract void OnApplyPowerUp(GameObject target);

    //public abstract void OnRemovePowerUp(GameObject target);

    public void OnActivate(TankData target)
    {
        target.moveSpeed += speedModifier;
        target.health += healthModifier;
        target.maxHealth += maxHealthModifier;
    }

    public void OnDeactivate(TankData target)
    {
        target.moveSpeed -= speedModifier;
        target.health -= healthModifier;
        target.maxHealth -= maxHealthModifier;
    }

}
